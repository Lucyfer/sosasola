package models;

//import java.util.*;
import javax.persistence.*;
import play.data.validation.Required;
import play.db.jpa.*;

@Entity
public class Category extends Model {

    @Required
    public String title;

    public Category(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
