package models;

//import java.util.*;
import javax.persistence.*;
import play.db.jpa.*;
import play.data.validation.*;

@Entity
@Table(name="Users")
public class User extends Model {

    public String fullname;

    @Email
    @Required
    @Column(unique=true)
    @CheckWith(UniqueEmail.class)
    public String email;

    @Required
    public String password;
    
    @Required
    public boolean isAdmin;

    static class UniqueEmail extends Check {
        @Override
        public boolean isSatisfied(Object user, Object mail) {
            setMessage("validation.email.nonuniq", "Non-unique e-mail");
            return find("byEmail", mail).first() == null;
        }
    }

    public User(String fullname, String email, String password, boolean isAdm) {
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.isAdmin = isAdm;
    }

    public User() {}

    public static User connect(String email, String password) {
        return find("byEmailAndPassword", email, password).first();
    }

    @Override
    public String toString() {
        return String.format("%s [%s]", email, fullname);
    }
}