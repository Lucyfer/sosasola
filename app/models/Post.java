package models;

import java.util.*;
import javax.persistence.*;
import play.data.validation.*;
import play.db.jpa.*;

@Entity
public class Post extends Model {

    @Required
    public String title;

    @Required
    @Temporal(TemporalType.TIMESTAMP)
    public Date postedAt;

    @Required
    public boolean approved;

    @Column(columnDefinition="TEXT NOT NULL")
    @Required
    @MaxSize(2000)
    public String description;

    @Column(columnDefinition="TEXT NOT NULL")
    @Required
    @MaxSize(20000)
    @Basic(fetch=FetchType.LAZY)
    public String content;

    @ManyToOne
    @Required
    public User author;

    @ManyToOne
    @Required
    public Category category;

    //@OneToMany(mappedBy="post", cascade=CascadeType.ALL)
    //public List<Comment> comments;

    public Post() {
        this.postedAt = new Date();
        this.title = "";
        this.description = "";
        this.approved = false;
        this.content = "";
        this.author = null;
        this.category = null;
    }
    
    public Post(User author, String title, String descr, String content, Category cat, boolean app) {
        this.title = title;
        this.description = descr;
        this.postedAt = new Date();
        this.content = content;
        this.author = author;
        this.category = cat;
        this.approved = app;
        //this.comments = new ArrayList<Comment>();
    }

    @Override
    public String toString() {
        return this.title;
    }
/*
    public Post addComment(String author, String content)
    {
        Comment c = new Comment(this, author, content).save();
        this.comments.add(c);
        this.save();
        return this;
    }
 */
}