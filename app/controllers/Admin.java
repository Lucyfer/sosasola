package controllers;

import play.mvc.*;
import models.*;

@Check("admin")
@With(Secure.class)
public class Admin extends Controller {

    public static void index() {
        render("CRUD/index.html");
    }
}
