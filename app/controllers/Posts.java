package controllers;

import models.Post;
import play.data.binding.Binder;
import play.mvc.*;
import play.data.validation.*;
import play.i18n.*;

import java.util.*;
import java.lang.reflect.*;
import java.lang.annotation.*;

@Check("admin")
@With(Secure.class)
public class Posts extends CRUD {

    public static void create() {
        Post object = new Post();
        Binder.bind(object, "object", params.all());
        object.postedAt = new Date();

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            render("Posts/blank.html", object);
        }
        object._save();
        flash.success(Messages.get("crud.created", "Post"));
        if (params.get("_save") != null) {
            redirect("Posts.list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect("Posts.blank");
        }
        redirect("Posts.show", object._key());
    }

    public static void save(long id) {
        Post object = Post.findById(id);
        Binder.bind(object, "object", params.all());

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            render("Posts/blank.html", object);
        }
        object._save();
        flash.success(Messages.get("crud.saved", "Post"));
        if (params.get("_save") != null) {
            redirect("Posts.list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect("Posts.blank");
        }
        redirect("Posts.show", object._key());
    }

    public static void show(long id) {
        Post p = Post.findById(id);
        if (p != null) {
            renderArgs.put("object", p);
            render();
        }
        else
            render("Posts/notfound.html");
    }
}
