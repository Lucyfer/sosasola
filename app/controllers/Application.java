package controllers;

//import play.*;
import org.hibernate.property.Dom4jAccessor;
import play.data.binding.Binder;
import play.data.validation.*;
import play.mvc.*;

import java.io.File;
import java.util.*;
import javax.persistence.*;
import models.*;

public class Application extends Controller {

    @Before
    static void setConnectedUser() {
        if (Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            if (user != null)
                renderArgs.put("user", (user.fullname == null || user.fullname.isEmpty()) ? user.email : user.fullname);
        }
    }

    @Before
    static void addDefaults() {
        renderArgs.put("categories", Category.findAll());
    }

    public static void blank() {
        render();
    }

    public static void index(int page) {
        page = (page > 0) ? page : 0;
        Long total = Post.count();
        int perPage = 10;
        List<Post> posts = Post.find("order by postedAt desc").from(page * perPage).fetch(perPage);
        render(posts, page, total, perPage);
    }

    public static void showPost(long id) {
        Post post = Post.findById(id);
        render(post);
    }

    public static void showCategory(long id, int page) {
        page = (page > 0) ? page : 0;
        int perPage = 10;
        Category cat = Category.findById(id);
        Long total = Post.count("category", cat);
        List<Post> posts = Post.find("category_id=? order by postedAt desc",
                cat != null ? cat.id : 0L).from(page * perPage).fetch(perPage);
        render(posts, page, total, perPage);
    }

    public static void search(String q) {
        render(q);
    }

    public static void register() {
        render();
    }

    public static void createPost() {
        render();
    }

    public static void savePost(List<File> attachments) {
        //for (String a : params.all().keySet()) System.out.print(a + " ");
        //System.out.println(params.getAll("attachments").length);
        if (attachments != null)
            //System.out.println(attachments.getPath());
            for(File a : attachments) {
                if (a != null) System.out.println(a.getName());
                else System.out.println(a);
            }
        else System.out.println(attachments);
        flash.success("Пост отправлен в песочницу");
        redirect("Application.createPost");
        //render();
    }

    public static void registerUser() {
        User newuser = new User();
        Binder.bind(newuser, "newuser", params.all());
        String confirmation = params.get("newuser.confirmation");
/*
        System.out.println("--------------");
        System.out.println(newuser.email);
        System.out.println(newuser.fullname);
        System.out.println(newuser.password);
        System.out.println(confirmation);
*/
        validation.valid(newuser);
        validation.equals(newuser.password, confirmation).key("validation.confirmation");
        if(validation.hasErrors()) {
            render("Application/register.html", newuser, confirmation);
        }
        else {
            newuser.save();
            render("Application/complete.html");
        }
    }

    public static void comingSoon() {
        render();
    }

}