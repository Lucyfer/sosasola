import play.*;
import play.jobs.*;
import play.test.*;

import models.*;

@OnApplicationStart
public class Bootstrap extends Job {

    @Override
    public void doJob() {
        if (User.count() == 0)
            new User("admin", "admin@local.ru", "123456", true).save();
    }
}