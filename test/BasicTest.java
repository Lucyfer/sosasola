
import org.junit.*;
import java.util.*;
import play.test.*;
import models.*;
import play.data.validation.*;
import play.data.validation.Error;

public class BasicTest extends UnitTest {

    @Before
    public void setup() {
        //Fixtures.deleteAll();
        Fixtures.deleteDatabase();
    }

    @Test
    public void userModel() {
        Fixtures.loadModels("data.yml");

        assertEquals(2, User.count());
        assertNotNull(User.connect("юзер@почта.рф", "пороль"));
        assertNotNull(User.connect("bob@gmail.com", "secret"));
        assertNull(User.connect("bob@gmail.com", "invalid"));
        assertNull(User.connect("john@gmail.com", "secret"));

        User u = new User("sosnitski", "bob@gmail.com", "passw", false);
        Validation.valid("email", u);
        if(Validation.hasErrors()) {
            System.out.println(Validation.errors());
            for(Error e : Validation.errors()) {
                System.out.println(e.message());
            }
        }
        else System.out.println("no");
        //assertEquals(u.validateAndCreate(), false);
        //System.out.print(u.validateAndCreate());
    }
    
    @Test
    public void categoryModel() {
        Fixtures.loadModels("data.yml");
        
        assertNotNull(Category.find("byTitle", "Категория1").first());
    }
    
    @Test
    public void postModel() {
        Fixtures.loadModels("data.yml");
        
        User u = User.all().first();
        Category c = Category.all().first();
        new Post(u, "Заголовок", "Описание", "Текст", c).save();
        
        Post p = Post.find("byTitle", "Заголовок").first();
        assertNotNull(p);
        assertEquals(new Post(null, "", "", "", null).validateAndCreate(), false);
    }
}
