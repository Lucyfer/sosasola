fullname=Имя
email=E-mail
password=Пароль
isAdmin=Администратор?

validation.email.nonuniq=Этот адрес занят. Выберите другой.
validation.confirmation=Должно совпадать с паролем

secure.username=Email
secure.password=Пароль
secure.remember=Запомнить меня
secure.error=Вход не удался. Проверьте логин и пароль
secure.signin=Войти

title=Заголовок
approved=Проверено?
description=Краткое описание
content=Основное содержание
author=Автор
category=Категория

post.title=Заголовок
post.approved=Проверено?
post.description=Краткое описание
post.content=Основное содержание
post.author=Автор
post.category=Категория